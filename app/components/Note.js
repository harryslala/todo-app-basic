/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from "react-native";

var Swipeout = require("react-native-swipeout");

const swipeoutBtn = [{ text: "Button" }];

export default class ToDoApp extends Component {
  render() {
    return (
      <View key={this.props.keyval} style={styles.note}>
        <Text style={styles.noteText}>
          {this.props.val.note}
        </Text>
        <Text style={styles.noteTextDate}>
          {this.props.val.date}
        </Text>

        <TouchableOpacity
          onPress={this.props.deleteMethod}
          style={styles.noteDelete}
        >
          <Text style={styles.noteDeleteText}>X</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  note: {
    position: "relative",
    padding: 20,
    paddingRight: 50,
    borderBottomWidth: 2,
    borderBottomColor: "#ededed"
  },
  noteTextDate: {
    paddingLeft: 12,
    borderLeftWidth: 5,
    fontSize: 12,
    borderLeftColor: "#E91E63"
  },
  noteText: {
    paddingLeft: 12,
    borderLeftWidth: 5,
    borderLeftColor: "#E91E63",
    fontSize: 17,
    fontWeight: "bold"
  },
  noteDelete: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2980b9",
    padding: 10,
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: "white"
  }
});
