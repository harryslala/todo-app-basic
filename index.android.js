/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  AsyncStorage
} from "react-native";

import Note from "./app/components/Note";

export default class ToDoApp extends Component {
  state = {
    noteArray: [],
    noteText: ""
  };

  componentDidMount() {
    this.updateList();
  }

  render() {
    let notes = this.state.noteArray.map((val, key) => {
      return (
        <Note
          key={key}
          keyval={key}
          val={val}
          deleteMethod={() => this.deleteNote(key)}
        />
      );
    });

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}> - ToDoApp - </Text>
        </View>
        <ScrollView style={styles.scrollContainer}>
          {notes}
        </ScrollView>
        <View style={styles.footer}>
          <TouchableOpacity onPress={this.addNote} style={styles.addButton}>
            <Text style={styles.addButtonText}> + </Text>
          </TouchableOpacity>

          <TextInput
            style={styles.textInput}
            onChangeText={noteText =>
              this.setState({
                noteText
              })}
            value={this.state.noteText}
            placeholder="Ketik disini..."
            placeholderTextColor="white"
            multiline={true}
            underlineColorAndroid="transparent"
          />
        </View>
      </View>
    );
  }

  addNote = () => {
    const noteArray = this.state.noteArray.slice();
    if (this.state.noteText) {
      //jika ada inputan

      let months = new Array(12);
      months[1] = "Jan";
      months[2] = "Feb";
      months[3] = "Mar";
      months[4] = "Apr";
      months[5] = "May";
      months[6] = "Jun";
      months[7] = "Jul";
      months[8] = "Aug";
      months[9] = "Sep";
      months[10] = "Oct";
      months[11] = "Nov";
      months[12] = "Des";

      let d = new Date();
      let hour = d.getHours() - (d.getHours() >= 12 ? 12 : 0);
      let period = d.getHours() >= 12 ? "PM" : "AM";
      let dates =
        months[d.getMonth() + 1] +
        " " +
        d.getDate() +
        ", " +
        hour +
        ":" +
        d.getMinutes() +
        " " +
        period;

      /*d
          .toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit"
          })
          .replace(/(:\d{2}| [AP]M)$/, "");*/

      //replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");

      noteArray.unshift({
        date: dates,
        note: this.state.noteText
      });

      AsyncStorage.setItem("noteArray", JSON.stringify(noteArray), () => {
        this.updateList();
      }).catch(error => {
        console.log(error);
      });

      //this.saveNote();

      this.setState({
        noteText: ""
      });
    }
  };

  /*saveNote() {
    const noteArray = this.state.noteArray;

    AsyncStorage.setItem("noteArray", JSON.stringify(noteArray), () => {
      this.updateList();
    }).catch(error => {
      console.log(error);
    });
  }*/

  updateList() {
    AsyncStorage.getItem("noteArray", (error, result) => {
      const parsedList = JSON.parse(result);
      const noteArray = parsedList || [];

      this.setState({ noteArray: noteArray });
    });
  }

  deleteNote(key) {
    this.state.noteArray.splice(key, 1);
    /*const noteArray = this.state.noteArray.filter(
      (item, itemKey) => itemKey !== key
    );*/
    this.setState(
      {
        noteArray: this.state.noteArray
      },
      () => {
        AsyncStorage.setItem("noteArray", JSON.stringify(this.state.noteArray));
      }
    ); //memperbarui dan menyimpan noteArray setelah melakukan penghapusan
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    backgroundColor: "#E91E63",
    alignItems: "flex-start",
    justifyContent: "center",
    borderBottomWidth: 5,
    borderBottomColor: "#ddd"
  },
  headerText: {
    color: "white",
    fontSize: 18,
    padding: 26
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100
  },
  footer: {
    position: "absolute",
    alignItems: "center",
    bottom: 0,
    left: 0,
    right: 0
  },
  addButton: {
    backgroundColor: "#E91E63",
    width: 60,
    height: 60,
    borderRadius: 20,
    borderWidth: 3,
    borderColor: "white",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    marginBottom: -32,
    zIndex: 1
  },
  addButtonText: {
    color: "#fff",
    marginBottom: 4,
    fontSize: 30
  },
  textInput: {
    alignSelf: "stretch",
    color: "#fff",
    padding: 20,
    paddingTop: 46,
    backgroundColor: "#252525",
    borderTopWidth: 2,
    borderTopColor: "#ededed"
  }
});

AppRegistry.registerComponent("ToDoApp", () => ToDoApp);
